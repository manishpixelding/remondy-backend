from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path, re_path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions
from rest_framework_simplejwt.views import (TokenObtainPairView,
                                            TokenRefreshView)
from service.account.views import (CookieTokenObtainPairView,
                                   CookieTokenRefreshView)

schema_view = get_schema_view(
    openapi.Info(
        title="Remondy System API",
        default_version='v1',
        description="API Documentation For Remondy System",
        terms_of_service="https://www.remondysystem.com/policies/terms/",
        contact=openapi.Contact(email="manish@pixelding.de"),
        license=openapi.License(name="Remondy System License"),
    ),
    public=True,
    permission_classes=[permissions.AllowAny],
)

urlpatterns = [
    path('admin/', admin.site.urls),

    path('api/token/', CookieTokenObtainPairView.as_view(),
         name='token_obtain_pair'),
    path('api/token/refresh/', CookieTokenRefreshView.as_view(), name='token_refresh'),

    path('api/account/', include('service.account.urls'),),
    path('api/order/', include('service.order.urls')),
    path('api/pricing/', include('service.pricing.urls'), name="pricing"),


    re_path(r'^swagger(?P<format>\.json|\.yaml)$',
            schema_view.without_ui(cache_timeout=0), name='schema-json'),

    re_path(r'^swagger/$', schema_view.with_ui('swagger',
            cache_timeout=0), name='schema-swagger-ui'),

    re_path(r'^redoc/$', schema_view.with_ui('redoc',
            cache_timeout=0), name='schema-redoc'),

]


if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL,
                          document_root=settings.STATIC_ROOT)
