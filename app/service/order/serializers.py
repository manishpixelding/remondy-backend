
from rest_framework.serializers import ImageField, ModelSerializer
from service.order.models import Order


class OrderSerializer(ModelSerializer):
    damage_image = ImageField(
        max_length=None, use_url=True, allow_null=True, required=False)

    class Meta:
        model = Order
        fields = ['id', 'customer', 'customer_name', 'address', 'device', 'damage_image', 'damage_desc', 'shipper',
                  'depot', 'ammount', 'payment_status', 'created_at', 'updated_at', ]

    def get_image(self, obj):
        return self.context['request'].build_absolute_uri(obj.image.url)
