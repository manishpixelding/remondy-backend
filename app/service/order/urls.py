
from django.urls import path
from django.views.decorators.csrf import csrf_exempt
from service.order.views import (CreateOrderAPIView, GetOrdersByUserAPIView,
                                 OrdersAPIView)

urlpatterns = [
    path('', OrdersAPIView.as_view(), name="all-orders"),
    path('create', CreateOrderAPIView.as_view(), name='create-order'),
    path('user', GetOrdersByUserAPIView.as_view(),
         name="get-orders-by-user")
]
