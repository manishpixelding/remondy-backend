from django.db import models
from service.account.models import User

PAYMENT_STATUS_PENDING = 'P'
PAYMENT_STATUS_COMPLETE = 'C'
PAYMENT_STATUS_FAILED = 'F'
PAYMENT_STATUS_CHOICES = [
    (PAYMENT_STATUS_PENDING, 'Pending'),
    (PAYMENT_STATUS_COMPLETE, 'Complete'),
    (PAYMENT_STATUS_FAILED, 'Failed')
]


class Order(models.Model):
    customer = models.ForeignKey(User, on_delete=models.CASCADE)
    customer_name = models.CharField(max_length=250, null=False)
    address = models.CharField(max_length=255, null=False)
    device = models.CharField(max_length=255, null=False)
    shipper = models.CharField(max_length=255, null=False)
    depot = models.CharField(max_length=255, null=False)
    ammount = models.PositiveIntegerField(null=False, default=1)
    damage_image = models.FileField(
        upload_to="order/damage/", null=True, blank=True, default=None)
    damage_desc = models.TextField()
    payment_status = models.CharField(
        max_length=1, choices=PAYMENT_STATUS_CHOICES, default=PAYMENT_STATUS_PENDING)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.customer_name

    class Meta:
        app_label = 'order'
        verbose_name = "order"
        ordering = []


DELIVARY_STATUS_PENDING = 'P'
DELIVARY_STATUS_COMPLETE = 'C'
DELIVARY_STATUS_FAILED = 'F'

DELIVARY_STATUS_CHOICES = [
    (DELIVARY_STATUS_PENDING, 'Pending'),
    (DELIVARY_STATUS_COMPLETE, 'Complete'),
    (DELIVARY_STATUS_FAILED, 'Failed')
]


class Delivary(models.Model):
    order = models.ForeignKey(
        'Order', on_delete=models.CASCADE, related_name="order_delivary")
    delivary_status = models.CharField(
        max_length=1, choices=DELIVARY_STATUS_CHOICES, default=DELIVARY_STATUS_PENDING)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
