# src/users/admin.py
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .models import Order


class CustomOrdrAdmin(UserAdmin):
    model = Order
    # list_display = ('email', 'is_staff', 'is_active',)
    list_filter = ('payment_status', )
    # fieldsets = (
    #     (None, {'fields': ('email', 'password')}),
    #     ('Permissions', {'fields': ('is_staff', 'is_active')}),
    # )
    # add_fieldsets = (
    #     (None, {
    #         'classes': ('wide',),
    #         'fields': ('email', 'password1', 'password2', 'is_staff', 'is_active')}
    #      ),
    # )
    # search_fields = ('email',)
    # ordering = ('email',)


admin.site.register(Order)
