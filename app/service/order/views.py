from urllib import response

from django.middleware import csrf
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet
from service.account.authenticate import CustomAuthentication
from service.order.models import Order
from service.order.serializers import OrderSerializer


# Create your views here.
class OrderViewSet(ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    permission_classes = [IsAdminUser]


class OrdersAPIView(APIView):
    permission_classes = []

    def get(self, request):
        orders = Order.objects.all()
        serializer = OrderSerializer(orders, many=True)
        return Response(serializer.data)


class CreateOrderAPIView(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request):
        if request.data:
            order = request.data
            order['customer'] = request.user.pk
            order['damage_image'] = None
            print(order, request.data, self.get_token(request))
            serializer = OrderSerializer(data=request.data)
            serializer.is_valid(True)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            return Response(serializer.errors)
        return Response({"error": "error"})


class GetOrdersByUserAPIView(APIView):
    def get(self, request):
        orders = Order.objects.filter(customer=request.user.pk)

        serializer = OrderSerializer(orders, many=True)
        return Response(serializer.data)
