import email
import os
import string
from hashlib import new
from random import random

from django.conf import settings
from django.contrib.auth import authenticate
from django.core.mail import send_mail
from django.middleware import csrf
from django.shortcuts import get_object_or_404, render
from rest_framework import exceptions, status
from rest_framework.authentication import CSRFCheck
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework_simplejwt.exceptions import InvalidToken
from rest_framework_simplejwt.serializers import TokenRefreshSerializer
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework_simplejwt.views import (TokenObtainPairView,
                                            TokenRefreshView)
from service.account.authenticate import CustomAuthentication
from service.account.models import Reset, User
from service.account.serializers import UserSerializer


def get_tokens_for_user(user):
    refresh = RefreshToken.for_user(user)
    return {
        'refresh': str(refresh),
        'access': str(refresh.access_token),
    }


class CookieTokenRefreshSerializer(TokenRefreshSerializer):
    refresh = None

    def validate(self, attrs):
        attrs["refresh"] = self.context["request"].COOKIES.get("refresh")
        if attrs["refresh"]:
            return super().validate(attrs)
        else:
            raise exceptions.APIException(code=status.HTTP_400_BAD_REQUEST)


class CookieTokenObtainPairView(TokenObtainPairView):
    def finalize_response(self, request, response, *args, **kwargs):
        if response.data.get("refresh"):
            print(request.COOKIES)
            check = CSRFCheck(request)
            print(check.process_request(request))
            check.process_request(request)
            cookie_max_age = 3600 * 24 * 14  # 14 days
            response.set_cookie(
                "refresh",
                response.data["refresh"],
                max_age=cookie_max_age,
                httponly=True,
                samesite='None',
                secure=True,
                domain=os.environ.get("ZONE_NAME"),
            )
            response.set_cookie(
                "access",
                response.data["access"],
                max_age=cookie_max_age,
                httponly=True,
                samesite='None',
                secure=True,
                domain=os.environ.get("ZONE_NAME"),
            )
            del response.data["refresh"]
            del response.data["access"]
        return super().finalize_response(request, response, *args, **kwargs)


class CookieTokenRefreshView(TokenRefreshView):
    def finalize_response(self, request, response, *args, **kwargs):
        if response.data.get("refresh"):
            cookie_max_age = 3600 * 24 * 14  # 14 days
            response.set_cookie(
                "refresh",
                response.data["refresh"],
                max_age=cookie_max_age,
                samesite='None',
                httponly=True,
                secure=True,
                domain=os.environ.get("ZONE_NAME"),
            )
            response.set_cookie(
                "access",
                response.data["access"],
                max_age=cookie_max_age,
                httponly=True,
                samesite='None',
                secure=True,
                domain=os.environ.get("ZONE_NAME"),
            )
            del response.data["refresh"]
            del response.data["access"]
        return super().finalize_response(request, response, *args, **kwargs)

    serializer_class = CookieTokenRefreshSerializer


@api_view(["POST"])
@permission_classes([IsAuthenticated])
def logout(request):
    response = Response({"message": "logout successful"})
    response.delete_cookie("refresh")
    return response


# Create your views here.
class UserAPIView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        serializer = UserSerializer(request.user)
        return Response(serializer.data)


class GetAllUsersAPIView(APIView):

    def get(self, request):
        users = User.objects.all()
        serializer = UserSerializer(users, many=True)
        return Response(serializer.data)


class RegisterAPIView(APIView):
    authentication_classes = ()
    permissssion_classes = ()

    def post(self, request):
        data = {}
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            user = serializer.save()
            refresh = RefreshToken.for_user(user)
            data['response'] = "Registration Succesfull"
            data['token'] = {
                'refresh': str(refresh),
                'access': str(refresh.access_token)
            }
        else:
            data = serializer.errors
        return Response(data)


class ForgotAPIView(APIView):
    def post(self, request):
        email = request.data['email']
        token = ''.join(random.choice(string.ascii_lowercase +
                        string.digits) for _ in range(10))

        Reset.objects.create(email=request.data['email'], token=token)

        url = os.environ["CLIENT140_URL"] + 'reset/' + token

        send_mail(
            subject="Reset your password.",
            message=f'Click here <a href={url}></a> here to reset your password.',
            from_email="from@example.com",
            recipient_list=[email]
        )

        return Response({'message': 'success'})


class ResetAPIView(APIView):
    def post(self, request):
        data = request.data

        if data['password'] != data['password_confirm'] or data['password_confirm'] is None:
            raise exceptions.APIException("Password do not match")

        reset_password = Reset.objects.filter(token=data['token']).first()

        if not reset_password:
            raise exceptions.APIException("Invalid Link")

        user = User.objects.filter(email=reset_password.email).first()

        if not user:
            raise exceptions.APIException("User not found")

        user.set_password(data['password'])
        user.save()

        return Response({
            'message': 'success'
        })
