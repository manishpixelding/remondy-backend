
from django.urls import path
from service.account.views import (ForgotAPIView, GetAllUsersAPIView,
                                   RegisterAPIView, ResetAPIView, UserAPIView)

urlpatterns = [
    path('register', RegisterAPIView.as_view(), name='register'),
    path('forgot', ForgotAPIView.as_view(), name='forgot'),
    path('reset', ResetAPIView.as_view(), name='reset'),
    path('user', UserAPIView.as_view(), name='user'),
    path('user/all', GetAllUsersAPIView.as_view(), name='all_user')
]
