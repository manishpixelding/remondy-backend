# Create your models here.
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import gettext_lazy as _
from service.account.managers import CustomUserManager

# Create your models here.


class User(AbstractUser):
    email = models.CharField(_('Email address'), max_length=250, unique=True)
    first_name = models.CharField(_('Firstname'), max_length=250)
    last_name = models.CharField(_('Lastname'), max_length=250)
    password = models.CharField(_('Password'), max_length=250)
    is_maintainer = models.BooleanField(_('Is Maintainer'), default=False)
    is_customer = models.BooleanField(_('Is Customer'), default=True)
    username = None

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    def __str__(self):
        return self.email


class Reset(models.Model):
    email = models.CharField(max_length=250)
    token = models.CharField(max_length=255, unique=True)
