from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView
from service.pricing.models import Pricing
from service.pricing.serializers import PricingSerializer

# Create your views here.


class PricingAPIView(APIView):
    def get(self, request):
        pricings = Pricing.objects.all()
        serializer = PricingSerializer(pricings, many=True)
        return Response(serializer.data)


class CreatePricingAPIView(APIView):
    def post(self, request):
        serializer = PricingSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors)


class UpdatePricingAPIView(APIView):
    pass


class DeleteAPIView(APIView):
    pass
