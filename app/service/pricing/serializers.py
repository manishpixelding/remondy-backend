
from rest_framework.serializers import ModelSerializer
from service.pricing.models import Pricing


class PricingSerializer(ModelSerializer):
    class Meta:
        model = Pricing
        fields = ['id', 'title', 'base_price', ]
