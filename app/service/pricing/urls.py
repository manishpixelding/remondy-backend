
from django.urls import include, path, re_path
from service.pricing.views import CreatePricingAPIView, PricingAPIView

urlpatterns = [
    path('', PricingAPIView.as_view(), name='all-pricing'),
    path('create', CreatePricingAPIView.as_view(), name='create-pricing'),
]
