from django.db import models


# Create your models here.
class Pricing(models.Model):
    title = models.CharField(max_length=250)
    base_price = models.FloatField(default=0)

    def __str__(self):
        return self.title

    class Meta:
        app_label = 'pricing'
        verbose_name = "pricings"
